#include<stdio.h>
#include<string.h>
void swap(void *v[], int i, int j){
	void *t;
	t=v[i];v[i]=v[j];v[j]=t;
}
int acomp(char* a, char* b){
	return a[0]-b[0];
}
int dcomp(char* a, char* b){
	return b[0]-a[0];
}
int numcomp(int a, int b){
	return a-b;
}
void quicksort(void *v[], int left, int right, int (*comp)(void *, void*)){
	int i, last;
	if(left>=right) return;
	swap(v,left, (left+right)/2);
	last=left;
	for(i=left+1;i<=right;i++)
		if((*comp)(v[i],v[left])<0)
			swap(v,++last,i);
	swap(v,left,last);
	quicksort(v,left,last-1,comp);
	quicksort(v,last+1, right, comp);
}
void swap1(int& x, int& y) { 
	int t=x; 
	x=y; 
	y=t; 
}

int main(int argc, char *argv[]){
	int a=5;
	int b=6;
	swap1(a,b);
	printf("%d %d",a,b);
	//char* b[10]={"3","6","1","3","8","9","3","4","5","9"};
	//void* a[10]={8,3,7,4,9,2,8,3,7,4};
	//int i;
	//int* a= malloc(10*sizeof(int));
//	for(i=0;i<10;i++)
	//	*(a+i)=b[i];
	
	//quicksort(argv,1,argc-1,acomp);
//	swap(b,1,2);
	//for(i=1;i<argc;i++)
	//	printf("%s\n",argv[i]);
//	printf("%d",sizeof(char*));
	return 0;
}
