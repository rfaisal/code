


public class Utility {
	
	public static String strRev(String s) {
		int len=s.length();
		int half_len=len/2;
		char[] arr=s.toCharArray();
		for(int i=0;i<half_len;i++){
			char temp=arr[i];
			arr[i]=arr[len-i-1];
			arr[len-i-1]=temp;
		}
		return String.valueOf(arr);
	}
	public static String strRev1(String s) {
		if(s.length()==1) return s;
		else return strRev1(s.substring(1))+s.charAt(0);
	}
	public static boolean isDuplicateChars(String s){
		boolean [] hash = new boolean[128];
		for(int i=0;i<s.length();i++){
			if(hash[s.charAt(i)]) return false;
			else hash[s.charAt(i)]=true;
		}
		return true;
		
	}
	
}

