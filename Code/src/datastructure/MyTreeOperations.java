package datastructure;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.print.attribute.IntegerSyntax;
import javax.swing.tree.TreeNode;

public class MyTreeOperations {
   
	public static void preorder(TreeN t, ArrayList<Integer> result){
		if(t==null)
			return;
		result.add(t.value);
		preorder(t.left,result);
		preorder(t.right,result);
		
		
	}
	public static void swap(Integer a, Integer b){
		a=6;
		b=8;
	}
	public static int nextBigger(int []arr,int k){
		boolean begin=false;
		for(int i=0;i<arr.length;i++){
			if(!begin&&arr[i]==k){
				begin=true;
			}
			else if(begin&&arr[i]>k){
				return arr[i];
			}
		}
		return -1;
	}
	public static void main(String args[]){
		Integer a=5;
		Integer b=6;
		ArrayList<Integer> c= new ArrayList<Integer>();
		swap(a,b);
		//System.out.print(a+" "+b);
		StringBuffer sb= new StringBuffer("Hi");
		sb.setLength(21);
		sb.setCharAt(20, 'a');
		sb.setCharAt(15, '\0');
		
		System.out.print(sb.toString());
		
		ArrayList<Integer> in= new ArrayList<Integer>();
		in.add(2);
		in.add(5);
		in.add(0);
		in.add(3);
		in.add(-67);
		in.add(9);
		in.add(7);
		in.add(11);
		in.add(-2);
		in.add(88);
		in.add(2);
		int []aaaa= new int[]{2,5,3,4,6,1};
		System.out.print(nextBigger(aaaa,10));
	}
	public static ArrayList<Integer> heap_sort(ArrayList<Integer> heap){
		ArrayList<Integer> ret = new ArrayList<Integer>();
		make_heap(heap);
		while(heap.size()>1){
			ret.add(heap.get(0));
			heap.set(0, heap.remove(heap.size()-1));
			heap_down(heap);
		}
		ret.add(heap.remove(0));
		return ret;
		
	}
	public static void make_heap(ArrayList<Integer> in){
		for(int i=0;i<in.size();i++)
			heap_up(in,i);
	}
	public static void heap_down(ArrayList<Integer> in){
		int index=0;
		while(index<in.size()-1){
			int max_child;
			if(2*index>in.size()-1){
				//nochild
				break;
			}
			else if(2*index+1>in.size()-1){
				//no right child
				max_child=2*index;
			}
			else{
				//both child
				if(in.get(2*index)>in.get(2*index+1))
					max_child=2*index;
				else
					max_child=2*index+1;
			}
			if(in.get(index)<in.get(max_child)){
				int temp=in.get(index);
				in.set(index,in.get(max_child));
				in.set(max_child,temp);
				index=max_child;
			}
			else{
				break;
			}
		}
	}
	public static void heap_up(ArrayList<Integer> in, int index){
		while(index>0){
			int parent=index/2;
			int p_value=in.get(parent);
			int c_value=in.get(index);
			if(p_value<c_value){
				in.set(parent,c_value);
				in.set(index, p_value);
				index=parent;
			}
			else{
				break;
			}
		}
	}
	public static ArrayList<Integer> quick_sort(ArrayList<Integer> input){
		if(input.size()==1 || input.size()==0)
			return input;
		ArrayList<Integer> a=new ArrayList<Integer>();
		ArrayList<Integer> b=new ArrayList<Integer>();
		int pivot=input.remove(0);
		while(input.size()>0){
			int rem=input.remove(0);
			if(rem>pivot)
				a.add(rem);
			else
				b.add(rem);
		}
		a=quick_sort(a);
		b=quick_sort(b);
		a.add(pivot);
		a.addAll(b);
		return a;
	}
	public static ArrayList<Integer> merge_sort(ArrayList<Integer> input){
		if(input.size()==1)
			return input;
		ArrayList<Integer> a=new ArrayList<Integer>();
		ArrayList<Integer> b=new ArrayList<Integer>();
		int halfSize=input.size()/2;
		int size=input.size();
		for(int i=0;i<halfSize;i++){
			a.add(input.get(i));
		}
		for(int i=halfSize;i<size;i++){
			b.add(input.get(i));
		}
		a=merge_sort(a);
		b=merge_sort(b);
		return merge(a,b);
	}
	public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
		ArrayList<Integer> ret= new ArrayList<Integer>();
		while(a.size()>0 && b.size()>0){
			if(a.get(0)>b.get(0))
				ret.add(a.remove(0));
			else
				ret.add(b.remove(0));
		}
		while(a.size()>0)
			ret.add(a.remove(0));
		while(b.size()>0)
			ret.add(b.remove(0));
		return ret;
	}
}






class TreeN{
	public TreeN left;
	public TreeN right;
	public int value;
}
