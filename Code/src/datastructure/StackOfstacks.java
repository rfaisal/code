package datastructure;

import java.util.Stack;


public class StackOfstacks {
	Stack<Stack<Entry>> sos;
	int size;
	int currentStackSize=0;
	public StackOfstacks(int size){
		sos= new Stack<Stack<Entry>>();
		sos.push(new Stack<Entry>());
		this.size=size;
	}
	
	public void push(Entry e){
		if(currentStackSize==size){
			sos.push(new Stack<Entry>());
			currentStackSize=0;
		}
		sos.peek().push(e);
		currentStackSize++;
	}
	public Entry pop(){
		Entry r= sos.peek().pop();
		currentStackSize--;
		if(currentStackSize==0){
			sos.pop();
			currentStackSize=size;
		}
		return r;
	}
	
}



