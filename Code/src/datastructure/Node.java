package datastructure;

public class Node {
	public Node next;
	public int value;
	public Node(int value){
		this.value=value;
	}
	
	public Node put(int v){
		Node n= new Node(v);
		Node cur=this;
		//do we need to check if cur is null or not?-No
		while(cur.next!=null){
			cur=cur.next;
		}
		cur.next=n;
		return n;
	}
	public Node get(int v){
		Node cur=this;
		while(cur!=null){
			if(cur.value==v)
				return cur;
			cur=cur.next;
		}
		return null;
	}
	public String toString(){
		StringBuffer r= new StringBuffer();
		Node cur=this;
		while(cur!=null){
			r.append(cur.value);
			if(cur.next!=null)
				r.append("-->");
			cur=cur.next;
		}
		return r.toString();
	}
}
