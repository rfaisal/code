package datastructure;

import java.util.HashMap;

public class CircularDuplicate {
	public static boolean isDuplicate(SNode s) throws Exception{
		HashMap<Integer, Boolean> table= new HashMap<Integer, Boolean>();
		SNode fast=s;
		SNode slow=s;
		if(fast.next==null)
			throw new Exception("not circular");
		fast=fast.next.next;
		table.put(slow.v, true);
		slow=slow.next;
		while(fast!=slow){
			if(fast==null || fast.next==null)
				throw new Exception("not circular");
			fast=fast.next.next;
			if(table.get(slow.v)!=null)
				return true;
			else
				table.put(slow.v, true);
			slow=slow.next;
		}
		fast=s;
		while(fast!=slow){
			if(table.get(slow.v)!=null)
				return true;
			else
				table.put(slow.v, true);
			slow=slow.next;
			fast=fast.next;
		}
		return false;
	}
	public static void main(String[] args) throws Exception{
		SNode s= new SNode(1);
		s.next=new SNode(2);
		s.next.next=new SNode(3);
		s.next.next.next=new SNode(4);
		s.next.next.next.next=new SNode(5);
		s.next.next.next.next.next=new SNode(1);
		s.next.next.next.next.next.next=new SNode(7);
		s.next.next.next.next.next.next.next=s.next.next;
		System.out.print(isDuplicate(s));
	}
}
class SNode{
	public SNode next;
	public int v;
	public SNode(int v){
		this.v=v;
	}
}
