package datastructure;

import java.util.ArrayList;

public class HashT {
	private int size;
	private  ArrayList[] table;
	public HashT(int size){
		this.size=size;
		table = new ArrayList[size];
		for(int i=0;i<size;i++){
			table[i]= new ArrayList<Entry>();
		}
	}
	
	private int hash(Integer i){
		return i.hashCode()%size;
	}
	@SuppressWarnings("unchecked")
	public boolean put(Entry e){
		ArrayList<Entry> currentChain= table[hash(new Integer(e.key))];
		for(int i=0;i<currentChain.size();i++){
			if(currentChain.get(i).value==e.value && currentChain.get(i).key==e.key)
				return false;
		}
		currentChain.add(e);
		return true;
	}
	public Entry get(int key){
		ArrayList<Entry> currentChain= table[hash(new Integer(key))];
		for(int i=0;i<currentChain.size();i++){
			if(currentChain.get(i).key==key)
				return currentChain.get(i);
		}
		return null;
	}
}

class Entry{
	public int key;
	public int value;
	public Entry(int key, int value){
		this.key=key;
		this.value=value;
	}
	
}