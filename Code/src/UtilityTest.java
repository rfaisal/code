import static org.junit.Assert.*;

import org.junit.Test;


public class UtilityTest {

	@Test
	public void testStrRev() {
		assertEquals("Aboosayt", Utility.strRev("tyasoobA"));
	}
	@Test
	public void testStrRev1() {
		assertEquals("Aboosayt", Utility.strRev1("tyasoobA"));
	}

	@Test
	public void testisDuplicateChars() {
		assertTrue(Utility.isDuplicateChars("tyasobA"));
		assertFalse(Utility.isDuplicateChars("tyasobyA"));
	}

}
