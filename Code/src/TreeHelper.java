
public class TreeHelper {
	public static void main(String[] args){
		TNode n=new TNode(1);
		n.right=new TNode(2);
		n.right.right=new TNode(3);
		print(n);
	}
	public static void print(MyTNode t){
		if(t==null)
			return;
		else{
			System.out.print("["+t.value+","+t.isVisited+"] ");
			print((MyTNode) t.left);
			print((MyTNode) t.right);
		}
	}
}
class TNode{
	public TNode left;
	public TNode right;
	public TNode parent;
	public int value;
	public TNode(int value){
		this.value=value;
	}
}

class MyTNode extends TNode{
	public boolean isVisited=false;
	public MyTNode(int value){
		super(value);
	}
}
